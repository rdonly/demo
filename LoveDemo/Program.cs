﻿
public class Program 
{
    public static void Main()
    {
        float x, y, a, b;
        for (y = 1.5f; y > -1.5f; y -= 0.1f)
        {
            for (x = -1.5f; x < 1.5f; x += 0.05f)
            {
                a = x * x + y * y - 1;
                b = a * a * a - x * x * y * y * y;
                Console.Write(b <= 0.0f ? '*' : ' ');
            }
        }
    }
}

