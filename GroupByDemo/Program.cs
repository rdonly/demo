﻿// See https://aka.ms/new-console-template for more information


List<Student> students = new List<Student>() {
    new Student(){Name="小明",Classroom="a班",Subjects="数学",Score=100},
    new Student(){Name="小彭",Classroom="a班",Subjects="数学",Score=100},
    new Student(){Name="小明",Classroom="a班",Subjects="语文",Score=90},
    new Student(){Name="小黄",Classroom="b班",Subjects="语文",Score=90}
};

var groupStudents = from student in students
                    group student by new { student.Name, student.Classroom }
                    into res
                    select new Student
                    {
                        Name = res.First().Name,
                        Classroom = res.First().Classroom,
                        Subjects = "已考科目总分",
                        Score = res.Sum(x => x.Score),
                    };


foreach (var groupStudent in groupStudents)
{
    Console.WriteLine($"{groupStudent.Name},{groupStudent.Classroom},{groupStudent.Subjects},{groupStudent.Score}");
}



public class Student
{
    /// <summary>
    /// 姓名
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// 班级
    /// </summary>
    public string Classroom { get; set; }
    /// <summary>
    /// 科目
    /// </summary>
    public string Subjects { get; set; }
    /// <summary>
    /// 分数
    /// </summary>
    public float Score { get; set; }

}
