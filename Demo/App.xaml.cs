﻿using Demo.View;
using Demo.ViewModel;
using System.Windows;

namespace TreeViewDemo
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            //TreeViewDemoView treeViewDemoView = new TreeViewDemoView();
            //TreeViewDemoViewModel treeViewDemoViewModel = new TreeViewDemoViewModel();
            //treeViewDemoView.DataContext = treeViewDemoViewModel;
            //treeViewDemoView.Show();

            DataGridDemoViewModel dataGridDemoViewModel = new DataGridDemoViewModel();
            DataGridDemoView dataGridDemoView = new DataGridDemoView();
            dataGridDemoView.DataContext = dataGridDemoViewModel;
            dataGridDemoView.Show();

            //ListViewDemoViewModel listViewDemoViewModel = new ListViewDemoViewModel();
            //ListViewDemoView listViewDemoView=new ListViewDemoView();
            //listViewDemoView.DataContext= listViewDemoViewModel;
            //listViewDemoView.Show();
        }
    }
}
