﻿using Demo.Base;
using Demo.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Demo.ViewModel
{
    public class ListViewDemoViewModel: NotifyPropertyObject
    {
        ObservableCollection<ListViewModel> _items= new ObservableCollection<ListViewModel>();
        public ObservableCollection<ListViewModel> Items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
                this.RaisePropertyChanged("Items");
            }
        }
        public ListViewDemoViewModel()
        {
            ListViewModel listView1=new ListViewModel();
            listView1.Name = "张三";
            listView1.Score = 90;
            Items.Add(listView1);

            ListViewModel listView2 = new ListViewModel();
            listView2.Name = "李四";
            listView2.Score = 90;
            Items.Add(listView2);

            ListViewModel listView3 = new ListViewModel();
            listView3.Name = "王五";
            listView3.Score = 90;
            Items.Add(listView3);

            ListViewModel listView4 = new ListViewModel();
            listView4.Name = "赵六";
            listView4.Score = 80;
            Items.Add(listView4);

            ListViewModel listView5 = new ListViewModel();
            listView5.Name = "测试";
            listView5.Score = 80;
            Items.Add(listView5);

            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(Items);
            PropertyGroupDescription groupDescription = new PropertyGroupDescription("Score");
            view.GroupDescriptions.Add(groupDescription);
        }  
    }
}
