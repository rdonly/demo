using Demo.Base;
using Demo.Model;
using System.Collections.ObjectModel;

public class TreeViewDemoViewModel: NotifyPropertyObject
{
    private ObservableCollection<TreeNode> m_TreeNodes = new ObservableCollection<TreeNode>();
    public ObservableCollection<TreeNode> TreeNodes
    {
        get
        {
            return m_TreeNodes;
        }
        set
        {
            m_TreeNodes = value;
            this.RaisePropertyChanged("TreeNodes");
            
        }
    }
    public TreeViewDemoViewModel() {
        
        TreeNode fruits = new TreeNode();
        fruits.Name = "水果";
        TreeNode apple = new TreeNode();
        apple.Name = "苹果";
        TreeNode Pear = new TreeNode();
        Pear.Name = "梨子";
        fruits.Children.Add(apple);
        fruits.Children.Add(Pear);

        TreeNode vegetables = new TreeNode();
        vegetables.Name = "蔬菜";
        TreeNode cabbage = new TreeNode();
        cabbage.Name = "卷心菜";
        TreeNode waterspinach = new TreeNode();
        waterspinach.Name = "空心菜";
        vegetables.Children.Add(cabbage);
        vegetables.Children.Add(waterspinach);

        TreeNode foods = new TreeNode();
        foods.Name = "食物";
        foods.Children.Add(fruits);
        foods.Children.Add(vegetables);

        TreeNodes.Add(foods);
    }
}