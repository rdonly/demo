﻿using Demo.Base;
using Demo.Model;
using System.Collections.ObjectModel;

namespace Demo.ViewModel
{
    public class DataGridDemoViewModel : NotifyPropertyObject
    {
        private ObservableCollection<DataGridModel> _dataGridModels = new ObservableCollection<DataGridModel>();
        public ObservableCollection<DataGridModel> Items
        {
            get { return _dataGridModels; }
            set
            {
                _dataGridModels = value;
                this.RaisePropertyChanged("Items");
            }
        }
        public DataGridDemoViewModel()
        {
            DataGridModel model1 = new DataGridModel();
            model1.name = "张三";
            model1.math = "100";
            Items.Add(model1);

            DataGridModel model2 = new DataGridModel();
            model2.name = "张三";
            model2.math = "88";
            Items.Add(model2);

            DataGridModel model3 = new DataGridModel();
            model3.name = "李四";
            model3.math = "87";
            Items.Add(model3);

            DataGridModel model4 = new DataGridModel();
            model4.name = "李四";
            model4.math = "98";
            Items.Add(model4);
        }
    }
}
