﻿using Demo.Base;
using System;

namespace Demo.Model
{
    public class DataGridModel : NotifyPropertyObject
    {
        private string _name;
        public string name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string _math;
        public string math
        {
            get { return _math; }
            set { _math = value; }
        }
        public string detail
        {
            get
            {
                return $"{name}的分数为{math}";
            }
        }
    }
}
