﻿using Demo.Base;
using System.Collections.ObjectModel;

namespace Demo.Model
{
    public class TreeNode : NotifyPropertyObject
    {
        private string m_Name;
        public string Name
        {
            get { return m_Name; }
            set
            {
                m_Name = value;
                this.RaisePropertyChanged(Name);
            }
        }
        private bool m_IsChecked = false;

        public bool IsChecked
        {
            get { return m_IsChecked; }
            set
            {
                m_IsChecked = value;
                this.RaisePropertyChanged("IsChecked");
            }
        }
        private ObservableCollection<TreeNode> m_Children = new ObservableCollection<TreeNode>();
        public ObservableCollection<TreeNode> Children
        {
            get
            {
                return m_Children;
            }
            set
            {
                m_Children = value;
                this.RaisePropertyChanged("Children");
            }
        }
    }
}
