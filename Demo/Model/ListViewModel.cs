﻿using Demo.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Model
{
    public  class ListViewModel: NotifyPropertyObject
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set 
            { 
                _name = value; 
                this.RaisePropertyChanged("Name");
            }
        }
        private int _score;
        public int Score
        {
            get { return _score; }
            set
            {
                _score = value;
                this.RaisePropertyChanged("Score");
            }
        }
    }
}
