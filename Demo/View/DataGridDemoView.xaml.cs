﻿using Demo.Model;
using Demo.ViewModel;
using NPOI.HWPF;
using NPOI.HWPF.UserModel;
using NPOI.XWPF.UserModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Demo.View
{
    /// <summary>
    /// DataGridDemoView.xaml 的交互逻辑
    /// </summary>
    public partial class DataGridDemoView : Window
    {
        private Point _startPoint;

        private DataGridRow _preRow;
        public DataGridDemoView()
        {
            InitializeComponent();
        }

        private void dataGrid_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    DataGrid dataGrid = sender as DataGrid;
                    //if (dataGrid.BeginEdit()) return;
                    _preRow = FindVisualParent<DataGridRow>(e.OriginalSource as FrameworkElement);
                    if (_preRow != null)
                    {
                        DragDrop.DoDragDrop(_preRow, _preRow.DataContext, DragDropEffects.Move);
                        dataGrid.SelectedItem = _preRow.DataContext;
                    }
                }
            }catch(Exception ex)
            {

            }
        }
        private T FindVisualParent<T>(UIElement element) where T : UIElement
        {
            UIElement parent = element;
            while (parent != null)
            {
                T correctlyTyped = parent as T;
                if (correctlyTyped != null)
                {
                    return correctlyTyped;
                }
                parent = VisualTreeHelper.GetParent(parent) as UIElement;
            }
            return null;
        }
        private void dataGrid_Drop(object sender, DragEventArgs e)
        {
            try
            {
                DataGridRow dr = FindVisualParent<DataGridRow>(e.OriginalSource as FrameworkElement);
                if (dr != null)
                {
                    int index = dr.GetIndex();
                    int preindex = _preRow.GetIndex();
                    ObservableCollection<DataGridModel> items = (this.DataContext as DataGridDemoViewModel).Items;
                    if (index > items.Count - 1) return;
                    DataGridModel item = null;
                    for (int i = 0; i < items.Count; i++)
                    {
                        if (i == preindex)
                        {
                            item = items[i]; break;
                        }
                    }
                    if (item != null)
                    {
                        items.RemoveAt(preindex);
                        items.Insert(index, item);
                    }
                }
            }
            catch(Exception ex)
            {

            }
        }

        private void dataGrid_PreviewMouseMove_1(object sender, MouseEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            List<string> str = new List<string>();
            string fileName = @"C:\Download\11.doc";
            using (var temp=File.OpenRead(fileName))
            {
                if (fileName.EndsWith(".doc"))
                {
                    var doc = new HWPFDocument(temp);
                   var range= doc.GetRange();
                    TableIterator tableIter = new TableIterator(range);
                    NPOI.HWPF.UserModel.Table table;
                    NPOI.HWPF.UserModel.TableRow row;
                    NPOI.HWPF.UserModel.TableCell cell;
                    while (tableIter.HasNext())
                    {
                        table = tableIter.Next();
                        int rowNum = table.NumRows;
                        for (int j = 0; j < rowNum; j++)
                        {
                            row = table.GetRow(j);
                            int cellNum = row.NumCells();
                            for (int k = 0; k < cellNum; k++)
                            {
                                cell = row.GetCell(k);
                                for (int i = 0; i < cell.NumParagraphs; i++)
                                {
                                    //输出单元格的文本  
                                    str.Add(cell.GetParagraph(i).Text);
                                }
                                //输出单元格的文本  
                                //System.out.println(cell.text().trim());
                            }
                        }
                    }

                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            List<string> str = new List<string>();
            string fileName = @"C:\Download\11.doc";
            Aspose.Words.Loading.LoadOptions loadOptions = new Aspose.Words.Loading.LoadOptions();
            loadOptions.LoadFormat = Aspose.Words.LoadFormat.Doc;
            Aspose.Words.Document document = new Aspose.Words.Document(fileName, loadOptions);
            document.Save(@"C:\Download\111.docx");
        }
    }
}
