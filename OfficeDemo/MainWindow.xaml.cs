﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Office.Interop.Word;

namespace OfficeDemo
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : System.Windows.Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private static object Nothing = System.Reflection.Missing.Value;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
                var doc = app.Documents.Open(@"C:\test.doc", ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing);
                app.Visible = false;
                app.Activate();
                doc.Activate();
                //添加文字
                object unite = Microsoft.Office.Interop.Word.WdUnits.wdStory;
                app.Selection.EndKey(ref unite, ref Nothing);
                string text = "测试文本";
                System.Drawing.Font font = new System.Drawing.Font("宋体", 10f, System.Drawing.FontStyle.Regular);
                var range = doc.ActiveWindow.Selection.Range;
                int length = doc.Characters.Count - 1;
                range.ParagraphFormat.LineSpacing = 1f;
                range.Text = text;
                range.Font.Name = font.Name;
                range.Font.Size = font.Size;
                range.InsertParagraphAfter();
                object notChange = Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges;
                doc.Close(ref notChange, ref Nothing, ref Nothing);
                //doc.Close();
                object saveOption = Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges;
                app.Quit(ref saveOption, ref Nothing, ref Nothing);
                //app.Quit();
            }catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
