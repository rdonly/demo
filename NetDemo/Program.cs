﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace UseApiTest
{
    class Program
    {
        public static string RequestIp = @"https://on-premises.mtwocloud.com:4443/itwo40demo/rel5.1/services";
        public static string Token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjZGOEVEMTRFOEZBNjU5NzA5Q0I2RDU5NEY0RkRERDI1OTlCOUQ0MzRSUzI1NiIsInR5cCI6IkpXVCIsIng1dCI6ImI0N1JUby1tV1hDY3R0V1U5UDNkSlptNTFEUSJ9.eyJuYmYiOjE2NjkyNjc5ODMsImV4cCI6MTY2OTMxMTE4MywiaXNzIjoiaHR0cHM6Ly9vbi1wcmVtaXNlcy5tdHdvY2xvdWQuY29tOjQ0NDMvaXR3bzQwZGVtby9yZWw1LjEvaWRlbnRpdHlzZXJ2ZXJjb3JlL2NvcmUiLCJhdWQiOiJodHRwczovL29uLXByZW1pc2VzLm10d29jbG91ZC5jb206NDQ0My9pdHdvNDBkZW1vL3JlbDUuMS9pZGVudGl0eXNlcnZlcmNvcmUvY29yZS9yZXNvdXJjZXMiLCJjbGllbnRfaWQiOiJpVFdPLkNsb3VkIiwic3ViIjoidGVzdCIsImF1dGhfdGltZSI6MTY2OTI2Nzk4MywiaWRwIjoibG9jYWwiLCJ1c2VyX2lkIjoiMzciLCJuYW1lIjoidGVzdCIsImVtYWlsIjoiIiwiYnVzaW5lc3NwYXJ0bmVyX2lkIjoiIiwiY29udGFjdF9pZCI6IiIsInBvcnRhbHRva2VuIjoiRmFsc2UiLCJqdGkiOiI2MEM3NUUzQzA4QUM5MDgwRDdGNjhDMTQ5RTlGNEI2NiIsImlhdCI6MTY2OTI2Nzk4Mywic2NvcGUiOlsiZGVmYXVsdCJdLCJhbXIiOlsiY3VzdG9tIl19.Mw5ZVndI3zpiRuAbFAeGR-tqRm1X3ANUZ2GSmEhxKHE9BeyiBtPL71hvwDN8vjxtiqp48YAMcGhwZiGXaPT7bnSQQmiY4m3vklsrI7J07KYtoWQrnrpBI2xQe6T1tTNX-cZca9NfL8lvtrL5JCH2p4PNXxocftZDHQMkEMW8s12qnI7zJOxBAAlFuFzo8QzHkx8IBkNCZg7AOdNCBgGZ510h3-iTrdQkTYzPqnLBDQrnt-FrPafS3VaXDimv6AOvSb-ZgwKqvztAU9o8IGVJ3qi7VOc21C7J-gOgX8YOpPIVfmT7m8INP45EgvvYLfD8vat4X_NZj-U-_QaVKpftyQ";

        public static string Context = "{\"dataLanguageId\":5," +
                    "\"language\":\"zh\"," +
                    "\"culture\":\"en-gb\"," +
                    "\"permissionObjectInfo\":null," +
                    "\"secureClientRole\":\"5D4JUZGxU3Ylcw5Jg9zj7c+TdI1gCWawDdu+qRGzn3vxRjE1idG+JV/lt/2iHJmjqOBEla34YC3Ev9cABIAgECrdhiVSj5Cy/tYiOQPx2Wgo7KCQ+T5hGzX7MBxUiPmld7UUq5VsACn8Cq7XBpgl1zvRnQlKmADBXTdCEiQgl0swqUG1lZlfJmhnga6nOjJF\"" +
                    "}";
        public static void Main(string[] args)
        {
            //GetData("/change/publicapi/main/1.0");

            UpdateFile("/basics/publicapi/upload/1.0/upload", @"C:\1.txt");

            //BindingFile("/documents/publicapi/projectdocument/2.0");
            //Console.Read();
            //string filePath = @"C:\Users\Administrator\Desktop\1.txt";
            //ChangeTest(filePath);
            Console.Read();
        }

        public async static void ChangeTest(string filePath)
        {
            await ChangeFile(filePath, 1000021, "EPC-200172334");
        }
        public async static Task<bool> ChangeFile(string filePath,int projectId,string projectCode)
        {
            try
            {
                //if (!await InstanceManager.LoginManager.Login())
                //    return false;
                //ChangeInfoResponse changeInfoResponse = await InstanceManager.UpLoadManager.CreateChange(projectId, projectCode);
                //if (changeInfoResponse == null)
                //    return false;
                //UploadFileResponse uploadFileResponse = await InstanceManager.UpLoadManager.UploadFile(filePath);
                //if (uploadFileResponse == null)
                //    return false;
                //if (!await InstanceManager.UpLoadManager.BindFileToProject(uploadFileResponse.FileId, projectId))
                //    return false;
                //if (!await InstanceManager.UpLoadManager.ChangeStatus(changeInfoResponse.ChangeId, changeInfoResponse.ChangeStatusId))
                //    return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return true;
        }
        public async static Task<object> GetData(string url)
        {
            url = RequestIp + url;
            using (HttpClient httpClient = new HttpClient())
            {

                //httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Token);
                httpClient.DefaultRequestHeaders.Add("client-context", Context);
                //url = "https://on-premises.mtwocloud.com:4443/itwo40demo/rel5.1/services/basics/publicapi/company/1.0/checkcompanycode?requestedSignedInCompanyCode=904";
                Uri uri = new Uri(url);

                var reponse =await httpClient.GetAsync(url);

                //var data = reponse.Content.ReadAsStringAsync().Result;

                //object obj = JsonConvert.DeserializeObject(data);
                object obj = null;
                return obj;
            }
        }

        public static void UpdateFile(string url, string filePath)
        {
            UpdateFileAsync(url, filePath);
        }
        public static Task<object> UpdateFileAsync(string url,string filePath)
        {
            url = RequestIp + url;
            using (HttpClient httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Token);
                httpClient.DefaultRequestHeaders.Add("client-context", Context);
                Uri uri= new Uri(url);
                FileInfo fileInfo = new FileInfo(filePath);
                byte[] fileBytes = new byte[fileInfo.Length];
                FileStream stream = fileInfo.OpenRead();
                stream.Read(fileBytes, 0, Convert.ToInt32(stream.Length));
                stream.Close();

                //Dictionary<string, string> content = new Dictionary<string, string>();
                //content.Add("sectionType","temp");
                //content.Add("fileName", "22442.txt");
                //HttpContent valueContent = new FormUrlEncodedContent(content);

                var boudary = $"------WebKitFormBoundarye{DateTime.Now.Ticks.ToString("x")}";
                MultipartFormDataContent httpContent = new MultipartFormDataContent(boudary);
                httpContent.Headers.ContentType = MediaTypeHeaderValue.Parse($"multipart/form-data; boundary={boudary}");
                httpContent.Headers.ContentDisposition = ContentDispositionHeaderValue.Parse("form-data; name=\"file\";filename=\"123\"");
                var fileContent = new ByteArrayContent(fileBytes);
                httpContent.Add(new StringContent("temp"),"sectionType");
                httpContent.Add(new StringContent("123"), "fileName");
                httpContent.Add(fileContent, "file", "123.png");



                //fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue($"form-data")
                //{
                //    Name = "file",
                //    FileName = "1.txt"
                //};
                //(httpContent as MultipartFormDataContent).Add(new StringContent("{\"temp\"}", Encoding.UTF8, "application/json"), "\"sectionType\"");
                //(httpContent as MultipartFormDataContent).Add(new StringContent("{\"1.txt\"}", Encoding.UTF8, "application/json"), "\"fileName\"");
                //(httpContent as MultipartFormDataContent).Add(new StreamContent(new MemoryStream(fileBytes)), "file", "1.txt");
                //(httpContent as MultipartFormDataContent).Add(fileContent, "\"file\"", "\"" + HttpUtility.UrlEncode("1.txt") + "\"");


                var data = httpClient.PostAsync(uri,httpContent).Result;
                data.EnsureSuccessStatusCode();
                if (data.IsSuccessStatusCode)
                {
                    data.EnsureSuccessStatusCode();
                }
                return null;
            }

            return null;

        }

        public static void BindingFile(string url)
        {
            BindingFileAsync(url);
        }
        public static async Task<object> BindingFileAsync(string url)
        {
            url = RequestIp + url;
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Token);
                httpClient.DefaultRequestHeaders.Add("client-context", Context);
                Uri uri = new Uri(url);

                //FileInfo fileInfo = new FileInfo(filePath);
                //byte[] fileBytes = new byte[fileInfo.Length];
                //FileStream stream = fileInfo.OpenRead();
                //stream.Read(fileBytes, 0, Convert.ToInt32(stream.Length));
                //stream.Close();

                //Dictionary<string, string> content = new Dictionary<string, string>();
                //content.Add("sectionType","temp");
                //content.Add("fileName", "22442.txt");
                //HttpContent valueContent = new FormUrlEncodedContent(content);
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("BasFileArchiveDocId", "1000250");
                dic.Add("ProjectId", "1000021");
                HttpContent httpContent = new FormUrlEncodedContent(dic);

                //HttpContent httpContent = new MultipartFormDataContent();
                //(httpContent as MultipartFormDataContent).Add(new StringContent("1000250"), "BasFileArchiveDocId");
                //(httpContent as MultipartFormDataContent).Add(new StringContent("1000021"), "ProjectId");
                //(httpContent as MultipartFormDataContent).Add(new StreamContent(new MemoryStream(fileBytes)), "file");


                var data = await httpClient.PostAsync(uri, httpContent).ConfigureAwait(false);
                if (data.IsSuccessStatusCode)
                {
                    data.EnsureSuccessStatusCode();
                }
                return data;
            }

            return null;

        }
    }
}
